module gitlab.com/kvgo/diagrams

go 1.15

require (
	cloud.google.com/go v0.82.0
	github.com/blushft/go-diagrams v0.0.0-20201006005127-c78c821223d9
	google.golang.org/api v0.46.0
	google.golang.org/genproto v0.0.0-20210517163617-5e0236093d7a
	k8s.io/apimachinery v0.21.1
	k8s.io/client-go v0.21.1
)
