package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"

	htmltemplate "html/template"
	texttemplate "text/template"

	container "cloud.google.com/go/container/apiv1"
	"github.com/blushft/go-diagrams/diagram"
	"github.com/blushft/go-diagrams/nodes/k8s"
	option "google.golang.org/api/option"
	containerpb "google.golang.org/genproto/googleapis/container/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubernetes "k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"k8s.io/client-go/tools/clientcmd"
)

const prod string = "production"
const staging string = "staging"
const dev string = "development"

var htmlTpl *htmltemplate.Template
var resourceTpl *texttemplate.Template

type NameSpace struct {
	Pods        []string
	Services    []string
	Deployments []string
}

type Node struct {
	Name       string
	InternalIP string
	ExternalIP string
}

type Pod struct {
	PodName  string
	AppLabel string
	EnvLabel string
	Pod_IP   string
	Node     string
}

type Svc struct {
	SvcName  string
	AppLabel string
	EnvLabel string
	SvcIP    string
	Port     int32
}

// Parse the templates
func init() {
	htmlTpl = htmltemplate.Must(htmltemplate.ParseGlob("*.gohtml"))
	//resourceTpl = texttemplate.Must(texttemplate.ParseGlob("*.tmpl"))
}

func main() {

	//http.Handle("/", http.FileServer(http.Dir(".")))
	//http.HandleFunc("/production", productionEndpoint)
	//http.HandleFunc("/staging", stagingEndpoint)
	//http.ListenAndServe(":80", nil)
	//
	//http.Handle("/go-diagrams/", http.StripPrefix("/go-diagrams/", http.FileServer(http.Dir("./go-diagrams"))))

	//getClusters()
	//getNodes()
	//runApp()
	// Get the namespaces
	// getNamespaces calls getPods, getServices,
	getNamespaces()

	//getNodes()
	//getPods("alco-chamber")
	//getSvc("alco-chamber")
	//workingDir, err := os.Getwd()
	//checkError(err)
	//genDiagram(workingDir, "provision", getPods("provision"), getSvc("provision"))

}

func productionEndpoint(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")

	//diagramDir, err := os.Open(".go-diagrams")
	//checkError(err)

	dir, err := os.Open("./go-diagrams")
	checkError(err)
	defer dir.Close()

	files, err := dir.Readdirnames(-1)
	checkError(err)

	testData := map[string]string{
		"siteTitle": "Digramz-" + prod,
		"fileName":  "./go-diagrams/" + files[2],
	}

	err1 := htmlTpl.ExecuteTemplate(w, "index.gohtml", testData)
	checkError(err1)
}

func stagingEndpoint(w http.ResponseWriter, req *http.Request) {
	//w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	//
	//testData := map[string]string{
	//	"data1": "IT WORKS",
	//}
	//
	//err := htmlTpl.ExecuteTemplate(w, "index.gohtml", testData)
	//checkError(err)
}

// Gets all Namespaces and returns a slice(array)
func getNamespaces() {
	// Store namespaces in slice(array)
	nameSpaces := []string{}

	// config, err := clientcmd.BuildConfigFromFlags("", `C:\Users\Kevaughn\.kube\config`) -- used in Windows local dev env
	// Read in kube config
	kubeConfig := filepath.Join(os.Getenv("HOME"), ".kube", "config")
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	checkError(err)

	// Create client
	clientSet, err := kubernetes.NewForConfig(config)
	checkError(err)

	// Make api call to cluster
	api, err := clientSet.CoreV1().Namespaces().List(context.Background(), meta.ListOptions{})
	checkError(err)

	// Loop over api results, obtain only the "name" of the namespace, add namespaces to slice(array)
	for _, n := range api.Items {
		nameSpaces = append(nameSpaces, n.ObjectMeta.Name)
	}

	// Loop over each namespace and get pods, service etc
	for _, n := range nameSpaces {
		//getPods(n)
		//getSvc(n)
		runApp(n)
		//runApp(n, staging)
	}
}

// runApp takes in a namespace
func runApp(namespace string) {
	workingDir, err := os.Getwd()
	checkError(err)
	//requiredDir := "go-diagrams"

	//fmt.Println("WORKINGDIR", workingDir, "\n")

	genDiagram(workingDir, namespace, dev, getNodes(), getPods(namespace), getSvc(namespace))

	//Delete dir if it exists, otherwise generate diagram
	//if existCheck(workingDir, requiredDir) {
	//	// change to working directory
	//	os.Chdir(workingDir)
	//	// delete go-diagrams directory
	//	os.RemoveAll(requiredDir)
	//	genDiagram(workingDir, namespace, prod, getPods(namespace), getSvc(namespace))
	//	//genDiagram(workingDir, namespace, staging, getPods(namespace), getSvc(namespace))
	//} else {
	//	genDiagram(workingDir, namespace, prod, getPods(namespace), getSvc(namespace))
	//	//genDiagram(workingDir, namespace, staging, getPods(namespace), getSvc(namespace))
	//}

}

func genDiagram(dir1 string, namespace string, enviornment string, nodes []Node, pods []Pod, svcs []Svc) {
	//Check for empty slices(arrays) -- if pods or svcs are empty, dont generate a diagram
	if len(pods) <= 0 {
		return
	}
	if len(svcs) <= 0 {
		return
	}

	d1, err := diagram.New(
		diagram.Filename(namespace+"-"+dev),
		//diagram.Label(namespace+"-"+prod),
		diagram.Label(
			namespace+"-"+dev+"\n"+
				"\n"+
				nodes[0].Name+"\n"+
				nodes[0].InternalIP+"(Internal_IP)"+"\n"+
				nodes[0].ExternalIP+"(External_IP)"),
		diagram.Direction("LR"))
	checkError(err)

	//svc1 := k8s.Network.Svc(diagram.NodeLabel("TRAEFIK"))
	//ing1 := k8s.Network.Ing(diagram.NodeLabel("TRAEFIK"))
	//svc2 := k8s.Network.Svc(diagram.NodeLabel("<SVC_Name>"))
	//pod1 := k8s.Compute.Pod(diagram.NodeLabel("<Pod_Name>"))
	//pod2 := k8s.Compute.Pod(diagram.NodeLabel("<Pod_Name>"))
	//pod3 := k8s.Compute.Pod(diagram.NodeLabel("<Pod_Name>"))
	//ing1 := k8s.Network.Ing(diagram.NodeLabel("Traefik"))

	// Parent Group
	//dc := diagram.NewGroup("GCP")

	// Child Groups
	ndGrp := diagram.NewGroup("Node").Label("GKE-Node")
	svcGrp := ndGrp.NewGroup("SERVICES").Label("SERVICES")
	pdGrp := ndGrp.NewGroup("PODS").Label("PODS")

	// Font options
	f := diagram.Font{
		Name:  "consolas",
		Size:  11,
		Color: "black",
	}

	//for _, clusterNodes := range nodes {
	//	ndGrp.Add(
	//		k8s.Infra.Node(diagram.Name(clusterNodes.Name)),
	//	)
	//}

	//ndGrp.Add(k8s.Infra.Node(
	//	diagram.Name("l"),
	//	diagram.NodeLabel(""),
	//	diagram.Width(.5),
	//	diagram.Height(.5),
	//))

	for _, service := range svcs {
		svcGrp.Add(k8s.Network.Svc(diagram.Name(service.AppLabel), diagram.NodeLabel(service.SvcName+"\n"+service.SvcIP), diagram.SetFontOptions(f)))
	}

	for _, namespacePods := range pods {
		pdGrp.Add(k8s.Compute.Pod(diagram.Name(namespacePods.AppLabel), diagram.NodeLabel(namespacePods.PodName+"\n"+namespacePods.Pod_IP), diagram.SetFontOptions(f), diagram.LabelLocation("t")))
	}

	//Loop over the svc array and pod array, match up the services and pods and connect the two objects based on kube label
	for _, x := range svcGrp.Nodes() {
		//fmt.Println(x.Options.Label)
		//fmt.Println(x.Options.Name)
		for _, y := range pdGrp.Nodes() {
			//fmt.Println(y.Options.Name)
			if x.Options.Name == y.Options.Name {
				// The trailing 'Group' method specifies what is drawn in the grp
				// the below d1.Connect connects the endpoints within the ndGrp group
				d1.Connect(x, y, diagram.Forward()).Group(ndGrp)
			}
			//fmt.Println(y.Options.Label)
		}
	}

	os.Chdir(dir1)
	if err := d1.Render(); err != nil {
		log.Fatal(err)
	}

	os.Chdir(dir1 + "/go-diagrams")
	filename := namespace + "-" + dev
	diagramDot := fmt.Sprintf("%s.dot", filename)
	command := exec.Command("/usr/local/bin/dot", "-Tpng", diagramDot, "-O")
	//command.Run()
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr
	cmd_err := command.Run()
	fmt.Println(cmd_err)
	//return cmd_err

	//dc.NewGroup("Pods").Label("Pod Layer").Add(
	//	pod1,
	//	pod2,
	//	pod3,
	//	//k8s.Compute.Pod(diagram.NodeLabel("PODS")),
	//	//k8s.Compute.Pod(diagram.NodeLabel("PODS")),
	//	//k8s.Compute.Pod(diagram.NodeLabel("PODS")),
	//).ConnectAllFrom(svc2.ID())

	//d1.Connect(svc1, svc2, diagram.Forward()).Group(dc)
}

// getPods retrieves relevant Pod data and returns a slice (array) of "pods"
func getPods(namespace string) []Pod {

	pods := []Pod{}

	kubeConfig := filepath.Join(os.Getenv("HOME"), ".kube", "config")
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	checkError(err)

	clientSet, err := kubernetes.NewForConfig(config)
	checkError(err)

	api, err := clientSet.CoreV1().Pods(namespace).List(context.Background(), meta.ListOptions{
		LabelSelector: "environment=" + dev,
	})
	checkError(err)

	// Loop over pod results, for each pod create a new Pod type to store relevant data
	for _, p := range api.Items {
		pd := Pod{
			p.ObjectMeta.Name,
			p.ObjectMeta.Labels["app"],
			p.ObjectMeta.Labels["environment"],
			p.Status.PodIP,
			p.Spec.NodeName,
		}

		pods = append(pods, pd)
		//if len(pods) <= 0 {
		//	fmt.Println("There are no pods in", namespace)
		//} else {
		//	fmt.Println("there are pods in", namespace)
		//}
	}
	//fmt.Println(pods)
	//fmt.Printf("%T\n", pods)

	return pods
}

// getSvc retrieves relevant Service data and returns a slice (array) of "services"
func getSvc(namespace string) []Svc {

	svcs := []Svc{}

	kubeConfig := filepath.Join(os.Getenv("HOME"), ".kube", "config")
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	checkError(err)

	clientSet, err := kubernetes.NewForConfig(config)
	checkError(err)

	api, err := clientSet.CoreV1().Services(namespace).List(context.Background(), meta.ListOptions{})
	checkError(err)

	//fmt.Println(api.Items)

	// Loop through the services and access info
	for _, s := range api.Items {
		//fmt.Println(s.ObjectMeta.Namespace, s.ObjectMeta.Name)
		// only retrieve the results corresponding to the app environment ~ staging, prod, dev
		if s.Spec.Selector["environment"] == dev {
			for p, _ := range s.Spec.Ports {
				service := Svc{
					s.Name,
					s.Spec.Selector["app"],
					s.Spec.Selector["environment"],
					s.Spec.ClusterIP,
					s.Spec.Ports[p].Port,
				}
				svcs = append(svcs, service)
				//Validate/check for namespaces that have no services
				//if len(svcs) <= 0 {
				//	fmt.Println("There are no services in", namespace)
				//} else {
				//	fmt.Println("There are services in", namespace)
				//}
			}
		}
	}
	//fmt.Println(svcs)
	return svcs
}

func getNodes() []Node {

	nodes := []Node{}

	kubeConfig := filepath.Join(os.Getenv("HOME"), ".kube", "config")
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	checkError(err)

	clientSet, err := kubernetes.NewForConfig(config)
	checkError(err)

	api, err := clientSet.CoreV1().Nodes().List(context.Background(), meta.ListOptions{})

	for _, n := range api.Items {
		nd := Node{
			n.ObjectMeta.Name,
			n.Status.Addresses[0].Address,
			n.Status.Addresses[1].Address,
		}
		nodes = append(nodes, nd)
	}
	//fmt.Println(nodes)
	return nodes
}

func getClusters() *containerpb.ListClustersResponse {
	// Connect to Google Cloud Platform using Service Account credentials file
	ctx := context.Background()
	c, err := container.NewClusterManagerClient(ctx, option.WithCredentialsFile("diagram_key.json"))
	checkError(err)

	req := &containerpb.ListClustersRequest{
		Parent: "projects/elabs-kubernetes/locations/-",
	}

	resp, err := c.ListClusters(ctx, req)
	checkError(err)
	//fmt.Println(resp.Clusters)
	return resp
}

// Check if the directory exists
func existCheck(path string, requiredDir string) bool {
	dirs, err := ioutil.ReadDir(path)
	checkError(err)

	for _, subDir := range dirs {
		if subDir.Name() == requiredDir {
			return true
		}
	}
	return false
}

// Function to check errors
func checkError(e error) {
	if e != nil {
		log.Println(e)
	}
}
